﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ObsStatus.Models
{
    internal sealed class GetCurrentSceneResponse : ObsApiBaseResponse
    {
        [JsonProperty(PropertyName = "name")]
        internal string Name { get; private set; }

        [JsonProperty(PropertyName = "sources")]
        internal IList<Source> Sources { get; private set; }
    }
}
