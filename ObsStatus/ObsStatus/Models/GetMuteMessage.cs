﻿using Newtonsoft.Json;

namespace ObsStatus.Models
{
    internal sealed class GetMuteMessage : ObsApiBaseMessage
    {
        internal GetMuteMessage(string source)
        {
            RequestType = RequestType.GetMute;
            Source = source;
        }

        [JsonProperty(PropertyName = "source", Required = Required.Always)]
        internal string Source { get; set; }
    }
}
