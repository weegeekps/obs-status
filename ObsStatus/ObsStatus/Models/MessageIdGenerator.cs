﻿namespace ObsStatus.Models
{
    internal sealed class MessageIdGenerator
    {
        private const int STARTING_MESSAGE_ID = 1;

        private static MessageIdGenerator instance;
        private int messageId = STARTING_MESSAGE_ID;

        private MessageIdGenerator() {}

        internal int NextMessageId()
        {
            return messageId++;
        }

        internal void Reset()
        {
            messageId = STARTING_MESSAGE_ID;
        }

        internal static MessageIdGenerator GetInstance()
        {
            return instance ?? (instance = new MessageIdGenerator());
        }
    }
}
