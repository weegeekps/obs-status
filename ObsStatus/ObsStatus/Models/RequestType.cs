﻿namespace ObsStatus.Models
{
    internal enum RequestType
    {
        GetCurrentScene,
        GetMute
    }
}
