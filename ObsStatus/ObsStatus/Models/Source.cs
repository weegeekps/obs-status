﻿using Newtonsoft.Json;

namespace ObsStatus.Models
{
    internal sealed class Source
    {
        [JsonProperty(PropertyName = "cy")]
        internal int Cy { get; private set; }

        [JsonProperty(PropertyName = "cx")]
        internal int Cx { get; private set; }

        [JsonProperty(PropertyName = "name")]
        internal string Name { get; private set; }

        [JsonProperty(PropertyName = "id")]
        internal int Id { get; private set; }

        [JsonProperty(PropertyName = "render")]
        internal bool Render { get; private set; }

        [JsonProperty(PropertyName = "locked")]
        internal bool Locked { get; private set; }

        [JsonProperty(PropertyName = "source_cx")]
        internal int SourceCx { get; private set; }

        [JsonProperty(PropertyName = "source_cy")]
        internal int SourceCy { get; private set; }

        [JsonProperty(PropertyName = "type")]
        internal string Type { get; private set; }

        [JsonProperty(PropertyName = "volume")]
        internal int Volume { get; private set; }

        [JsonProperty(PropertyName = "x")]
        internal int X { get; private set; }

        [JsonProperty(PropertyName = "y")]
        internal int Y { get; private set; }
    }
}
