﻿using Newtonsoft.Json;

namespace ObsStatus.Models
{
    internal sealed class GetMuteResponse : ObsApiBaseResponse
    {
        [JsonProperty(PropertyName = "name", Required = Required.Always)]
        internal string Name { get; private set; }

        [JsonProperty(PropertyName = "muted", Required = Required.Always)]
        internal bool Muted { get; private set; }
    }
}
