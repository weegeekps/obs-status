﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ObsStatus.Models
{
    internal abstract class ObsApiBaseMessage
    {
        internal ObsApiBaseMessage()
        {
            MessageId = MessageIdGenerator.GetInstance().NextMessageId().ToString();
        }

        [JsonProperty(PropertyName = "request-type", Required = Required.Always)]
        [JsonConverter(typeof(StringEnumConverter))]
        internal RequestType RequestType { get; set; }

        [JsonProperty(PropertyName = "message-id", Required = Required.Always)]
        internal string MessageId { get; private set; }

        internal string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
