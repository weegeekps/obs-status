﻿namespace ObsStatus.Models
{
    internal sealed class GetCurrentSceneMessage : ObsApiBaseMessage
    {
        internal GetCurrentSceneMessage()
        {
            RequestType = RequestType.GetCurrentScene;
        }
    }
}
