﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ObsStatus.Models
{
    internal abstract class ObsApiBaseResponse
    {
        [JsonProperty(PropertyName = "status", Required = Required.Always)]
        [JsonConverter(typeof(StringEnumConverter))]
        internal ResponseStatus Status { get; private set; }

        [JsonProperty(PropertyName = "message-id", Required = Required.Always)]
        internal string MessageId { get; private set; }

        [JsonProperty(PropertyName = "error")]
        internal string Error { get; private set; }
    }
}
