﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using ObsStatus.Models;

namespace ObsStatus.Services
{
    internal struct ObsWebsocketInfo
    {
        internal string Host;
        internal string Port;
    }

    internal sealed class ObsWebsocket : IDisposable
    {
        private readonly ObsWebsocketInfo info;
        private readonly MessageWebSocket messageWebSocket;

        // Since the OBS Websocket protocol fails to send back the type of the request in the response,
        //   we have to keep a record ourselves of what types correspond to which ID.
        private readonly Dictionary<string, RequestType> typeRecords;

        internal ObsWebsocket(ObsWebsocketInfo info, TypedEventHandler<MessageWebSocket, MessageWebSocketMessageReceivedEventArgs> receivedHandler)
        {
            this.info = info;
            typeRecords = new Dictionary<string, RequestType>();

            messageWebSocket = new MessageWebSocket();

            messageWebSocket.Control.MessageType = SocketMessageType.Utf8;

            messageWebSocket.MessageReceived += receivedHandler;
            messageWebSocket.Closed += MessageWebSocketOnClosed;
        }

        private async Task SendQuery(ObsApiBaseMessage message)
        {
            try
            {
                using (var dw = new DataWriter(messageWebSocket.OutputStream))
                {
                    typeRecords.Add(message.MessageId, message.RequestType);
                    dw.WriteString(message.ToJson());
                    await dw.StoreAsync();
                    dw.DetachStream();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine($"SendQuery failed. Reason: ${e.Message}");
            }
        }

        private void MessageWebSocketOnClosed(IWebSocket sender, WebSocketClosedEventArgs args)
        {
            Debug.WriteLine($"Websocket Closed. Code: ${args.Code} Reason: ${args.Reason}");
        }

        internal Task Connect()
        {
            var task = messageWebSocket.ConnectAsync(
                new Uri($@"ws://{info.Host}:{info.Port}")
            ).AsTask();

            return task;
        }

        internal Task QueryMuteStatus(string source)
        {
            var message = new GetMuteMessage(source);

            return SendQuery(message);
        }

        internal Task QueryCurrentScene()
        {
            var message = new GetCurrentSceneMessage();

            return SendQuery(message);
        }

        internal RequestType GetRequestType(string messageId)
        {
            return typeRecords[messageId];
        }

        public void Dispose()
        {
            messageWebSocket.Close(1000, string.Empty);
            messageWebSocket.Dispose();
        }
    }
}