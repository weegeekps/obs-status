﻿using System;
using System.Diagnostics;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Newtonsoft.Json.Linq;
using ObsStatus.Models;
using ObsStatus.Services;

namespace ObsStatus.Views
{
    public sealed partial class MainPage : Page
    {
        private const int TIMER_DURATION = 1000;

        private ObsWebsocket websocket;
        private DispatcherTimer timer;

        public bool IsMuted { get; private set; }
        public string CurrentScene { get; private set; }

        public MainPage()
        {
            InitializeComponent();
        }

        private void UpdateMuteStatus(bool newMuteStatus)
        {
            IsMuted = newMuteStatus;

            var brush = new SolidColorBrush();

            if (IsMuted)
            {
                brush.Color = Colors.Red;
                MuteStatusTextBlock.Text = "Muted!";
            }
            else
            {
                brush.Color = Colors.Black;
                MuteStatusTextBlock.Text = "Unmuted";
            }

            MuteGrid.Background = brush;
        }

        private void UpdateCurrentScene(string newScene)
        {
            CurrentScene = newScene;

            CurrentSceneTextBlock.Text = CurrentScene;
        }

        private async void HandleGetMuteResponse(GetMuteResponse response)
        {
            if (response == null)
            {
                throw new InvalidOperationException("HandleGetMuteResponse response parameter was null.");
            }

            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => UpdateMuteStatus(response.Muted));
        }

        private async void HandleGetCurrentSceneResponse(GetCurrentSceneResponse response)
        {
            if (response == null)
            {
                throw new InvalidOperationException("HandleGetCurrentSceneResponse response parameter was null.");
            }

            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => UpdateCurrentScene(response.Name));
        }

        private void MessageReceivedHandler(MessageWebSocket sender, MessageWebSocketMessageReceivedEventArgs args)
        {
            try
            {
                var dr = args.GetDataReader();
                dr.UnicodeEncoding = UnicodeEncoding.Utf8;
                var response = JObject.Parse(dr.ReadString(dr.UnconsumedBufferLength));

                if (!response.ContainsKey("message-id"))
                {
                    // We return because this is likely an event message we don't support nor do we care about.
                    return;
                }

                var type = websocket.GetRequestType(response["message-id"].Value<string>());

                switch (type)
                {
                    case RequestType.GetMute:
                        HandleGetMuteResponse(response.ToObject<GetMuteResponse>());
                        break;
                    case RequestType.GetCurrentScene:
                        HandleGetCurrentSceneResponse(response.ToObject<GetCurrentSceneResponse>());
                        break;
                    default:
                        throw new Exception("Unknown message ID.");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Failed to handle message. Reason: ${ex.Message}");
            }
        }

        private async void OnTick(object sender, object e)
        {
            await websocket.QueryMuteStatus("Mic/Aux");
            await websocket.QueryCurrentScene();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            timer = new DispatcherTimer
            {
                Interval = TimeSpan.FromMilliseconds(TIMER_DURATION)
            };

            timer.Tick += OnTick;

            websocket = new ObsWebsocket(
                new ObsWebsocketInfo
                {
                    Host = "localhost", Port = "50505"
                },
                MessageReceivedHandler);

            try
            {
                var connectTask = websocket.Connect();
                connectTask.ContinueWith(async _ =>
                {
                    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => timer.Start());
                });
            }
            catch (Exception ex)
            {
                var webErrorStatus = WebSocketError.GetStatus(ex.GetBaseException().HResult);
                Debug.WriteLine($"Connect Failed: ${webErrorStatus}");
            }
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);
            websocket?.Dispose();
            timer?.Stop();
        }
    }
}
