﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ObsStatus.Models;

namespace ObsStatusTests.Models
{
    [TestClass]
    public class MessageIdGeneratorTests
    {

        [TestMethod]
        public void GetInstance_ReturnsTheSameInstance()
        {
            var instance1 = MessageIdGenerator.GetInstance();
            var instance2 = MessageIdGenerator.GetInstance();

            Assert.AreEqual(instance1.GetHashCode(), instance2.GetHashCode());
        }

        [TestMethod]
        public void NextMessageId_ShouldIterate()
        {
            var gen = MessageIdGenerator.GetInstance();
            gen.Reset();
            Assert.AreEqual(1, gen.NextMessageId());
            Assert.AreEqual(2, gen.NextMessageId());
            Assert.AreEqual(3, gen.NextMessageId());
        }
    }
}
