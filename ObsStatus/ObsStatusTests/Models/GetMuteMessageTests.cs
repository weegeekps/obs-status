﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ObsStatus.Models;

namespace ObsStatusTests.Models
{
    [TestClass]
    public class GetMuteMessageTests
    {
        [TestMethod]
        public void GetMuteMessage_ContainsTypeAndSource()
        {
            var source = "Mic/Aux";

            var message = new GetMuteMessage(source);

            Assert.AreEqual(RequestType.GetMute, message.RequestType);
            Assert.AreEqual(source, message.Source);
        }
    }
}
